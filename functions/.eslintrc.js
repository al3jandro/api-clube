module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'google',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['tsconfig.json', 'tsconfig.dev.json'],
    tsconfigRootDir: __dirname,
    sourceType: 'module',
    ecmaVersion: 8,
  },
  ignorePatterns: [
    '/lib/**/*', // Ignore built files.
  ],
  plugins: ['@typescript-eslint', 'import'],
  rules: {
    'no-console': ['off'],
    'no-invalid-this': ['off'],
    'max-len': ['off'],
    'object-curly-spacing': ['off'],
    'quotes': ['error', 'single', {avoidEscape: true}],
    'indent': ['off'],
    'require-jsdoc': ['error', {
      'require': {
          'FunctionDeclaration': true,
          'MethodDefinition': false,
          'ClassDeclaration': false,
          'ArrowFunctionExpression': false,
          'FunctionExpression': false,
      },
    }],
    '@typescript-eslint/explicit-module-boundary-types': ['off'],
    '@typescript-eslint/no-explicit-any': ['off'],
    'semi': ['off'],
    'camelcase': ['off'],
    'arrow-parens': ['off'],
    'padded-blocks': ['off'],
    'no-multi-spaces': ['off'],
    'eol-last': ['off'],
    'new-cap': 0,
  },
};

