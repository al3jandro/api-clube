import cors from 'cors';
import morgan from 'morgan';
import express, {Application } from 'express';
import * as functions from 'firebase-functions';

import Auth from './routes/auth';
import Saldo from './routes/saldo';
import Maintenament from './routes/maintenament';

const app: Application = express();
app.use(morgan('tiny'));
app.use(cors( { origin: true }));
// app.use('/v2', app);
app.use(Auth);
app.use(Maintenament);
app.use(Saldo);

// const PORT = process.env.PORT || 4000;
// app.listen(PORT, () => {
//     console.log('Server is running on port', PORT);
// });

exports.api = functions.https.onRequest(app);