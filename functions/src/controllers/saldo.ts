import axios from 'axios';
import * as https from 'https';

const url = 'https://slv.condor.com.br/api-rc/thirdparty/gs/conta-cliente';

const agent = new https.Agent({ rejectUnauthorized: false });

export default class SaldoController {

    saldo = async (token: any) => {
        const headers = { 'Content-Type': 'application/json', 'authorization': `${token}` };
        return await axios.get(`${ url }/saldo`, { headers, httpsAgent: agent })
        .then(response =>  response.data)
        .catch(err => err);
    }

    extrato = async (body: any, token: any) => {
        const headers = { 'Content-Type': 'application/json', 'authorization': `${token}` };
        return await axios.get(
            `${ url }/extrato?dataInicio=${body.dataInicio}&dataFim=${body.dataFim}`,
            { headers, httpsAgent: agent },
        )
        .then(response =>  response.data)
        .catch(err => err);
    }
}

