import axios from 'axios';
import { config } from './../config';

const headers = {
    'x-api-key': config.xapi,
    'idvarejista': config.varejo,
    'Content-Type': 'application/json',
    'authorization': '',
};

export default class MaintenamentController {

    cep = (body: any = {}) => this._process('cep', body);
    loja = (body: any = {}) => this._process('loja', body);
    estado = (body: any = {}) => this._process('estado', body);
    cidade = (body: any = {}) => this._process('cidade', body);
    bairro = (body: any = {}) => this._process('bairro', body);

    genero = (body: any = {}) => this._process('genero', body);
    profissao = (body: any = {}) => this._process('profissao', body);
    estado_civil = (body: any = {}) => this._process('estadocivil', body);
    grupo_familiar = (body: any = {}) => this._process('grupofamiliar', body);

    fale_conosco = (body: any = {}) => this._process('faleconosco', body);
    send_conosco = (body: any = {}) => this._process('sendfaleconosco', body);
    tipo_pessoa = (body: any = {}) => this._process('tipopessoa', body);
    config = (body: any = {}) => this._process('configuracao', body);

    historico_compra = async (token: any) => {
        headers.authorization = `${token}`;
        return await axios.get(config.historico, { headers })
        .then(response =>  response.data)
        .catch(err => err);
    }

    numero_sorte = async (body: any, token: any) => {
        headers.authorization = `${token}`;
        return await axios.post(`${ config.url }/campanha`, body, { headers })
        .then(response =>  response.data)
        .catch(err => err);
    }

    private _process = async (url: string, body: any) => {
        return await axios.post(`${ config.url }/${url}`, body, { headers })
        .then(response =>  response.data)
        .catch(err => err);
    }
}