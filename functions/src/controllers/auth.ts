import axios from 'axios';
import { config } from '../config';

const headers = {
    'x-api-key': config.xapi,
    'idvarejista': config.varejo,
    'Content-Type': 'application/json',
    'authorization': '',
};

export default class AuthController {
    sign_in = async (body: any) => this._process('login', body);
    sign_up = async (body: any) => this._process('cadastrocliente', body);
    update_user = async (body: any) => this._process('alterarcliente', body);
    validate_cpf = async (body: any) => this._process('validacpf', body);
    forgot_password = async (body: any) => this._process('sendesqueciminhasenha', body);

    private _process = async (url: string, body: any) => {
        headers.authorization = body.token || '';
        return await axios.post(`${ config.url }/${url}`, body, { headers })
        .then(response =>  response.data)
        .catch(err => err);
    }
}
