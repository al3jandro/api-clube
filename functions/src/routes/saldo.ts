import express from 'express';
import SaldoController from '../controllers/saldo';

const saldo = express.Router();
const controller = new SaldoController();

saldo.get('/saldo', async (_req, res) => {
    const response = await controller.saldo(_req.headers['authorization']);
    return res.send(response);
});

saldo.get('/extrato', async (_req, res) => {
    const response = await controller.extrato(_req.query, _req.headers['authorization']);
    return res.send(response);
});

export default saldo;
