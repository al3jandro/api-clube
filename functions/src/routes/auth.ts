import express from 'express';
import AuthController from '../controllers/auth';

const auth = express.Router();
const controller = new AuthController();

auth.post('/sign-in', async (_req, res) => {
    const response = await controller.sign_in(_req.body);
    return res.send(response);
});

auth.post('/sign-up', async (_req, res) => {
    const response = await controller.sign_up(_req.body);
    return res.send(response);
});

auth.post('/update-user', async (_req, res) => {
    const response = await controller.update_user(_req.body);
    return res.send(response);
});

auth.post('/forgot-password', async (_req, res) => {
    const response = await controller.forgot_password(_req.body);
    return res.send(response);
});

auth.post('/validate-cpf', async (_req, res) => {
    const response = await controller.validate_cpf(_req.body);
    return res.send(response);
});

export default auth;