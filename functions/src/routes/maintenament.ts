import express from 'express';
import MaintenamentController from '../controllers/maintenament';

const maintenament = express.Router();
const controller = new MaintenamentController();

// Maintenament
maintenament.get('/grupo-familiar', async (_req, res) => {
  const response = await controller.grupo_familiar();
  return res.send(response);
});

maintenament.get('/estado-civil', async (_req, res) => {
  const response = await controller.estado_civil();
  return res.send(response);
});

maintenament.get('/fale-conosco', async (_req, res) => {
  const response = await controller.fale_conosco();
  return res.send(response);
});

maintenament.get('/profissao', async (_req, res) => {
  const response = await controller.profissao();
  return res.send(response);
});

maintenament.get('/loja', async (_req, res) => {
  const response = await controller.loja();
  return res.send(response);
});

maintenament.get('/estado', async (_req, res) => {
  const response = await controller.estado();
  return res.send(response);
});

maintenament.get('/cidade', async (_req, res) => {
  const response = await controller.cidade();
  return res.send(response);
});

maintenament.get('/bairro', async (_req, res) => {
  const response = await controller.bairro();
  return res.send(response);
});

maintenament.get('/tipo-pessoa', async (_req, res) => {
  const response = await controller.tipo_pessoa();
  return res.send(response);
});

maintenament.get('/config', async (_req, res) => {
  const response = await controller.config();
  return res.send(response);
});

maintenament.get('/historico-compra', async (_req, res) => {
  const response = await controller.historico_compra(_req.headers['authorization']);
  return res.send(response);
});

maintenament.post('/cep', async (_req, res) => {
  const response = await controller.cep(_req.body);
  return res.send(response);
});

maintenament.post('/send-conosco', async (_req, res) => {
  const response = await controller.send_conosco(_req.body);
  return res.send(response);
});

maintenament.post('/campanha', async (_req, res) => {
  const response = await controller.numero_sorte(
    _req.body, _req.headers['authorization']);
  return res.send(response);
});

export default maintenament;
